import numpy as np
import pandas as pd
import os, time, re ,json, itertools

# columns of raw trajectory data (flightradar24 directly)
flight_radar_columns = ["SSR", "latitude", "longitude", "direction", "altitude", "velocity", "squawk", "recevingRadar", \
                        "flightType", "flightID", "time", "origin", "destination", "flightID2", "unknown1", "unknown2", \
                        "callSign", "unknown3"]
# columns of cleaned data set
cleaned_data_columns = ["idf", "latitude", "longitude", "flightID", "origin", "destination", "time"]

def process_adsb(adsb_data_dir, date_list, odPair_list, store_dir, 
                    store_name='cleaned_data.csv', airpInfor_dir="", start_label=1):
    """ process raw adsb data into csv format
        adsb_data_dir:  directory of raw adsb data
        date_list:      list of date that would be processed
        odPair_list:    list of OD pairs, flights not in these OD pairs would be discarded
        store_dir:      store directory
        store_name:     store name
        airpInfor_dir:  if assigned a value, the geolocation of the OD pair would be added to the trajectory
        start_label:    start label of point_order
    """
    pre_time = time.time()

    clean_data_dict = {item: [] for item in cleaned_data_columns}
    ccount = 0
    for cdate in date_list:
        cdate = f'{cdate}'
        for fl in os.listdir(os.path.join(adsb_data_dir, cdate)):
            with open(os.path.join(adsb_data_dir, cdate, fl), 'r') as fd:
                trajectory_json = json.load(fd)
        
            for key, valList in trajectory_json.items():
                if key in ['full_count', 'version']:
                    continue

                origin, destination = valList[11], valList[12]
                cur_od = tuple(sorted([origin, destination]))
                if cur_od in odPair_list:
                    odPair = '-'.join([origin, destination])
                    flightid, regist = valList[13], valList[9]
                    identifier = f'{flightid}_({odPair}_{regist})'  # unique identifier

                    clean_data_dict['idf'].append(identifier)
                    for item in cleaned_data_columns[1:]:
                        clean_data_dict[item].append(valList[flight_radar_columns.index(item)])
        unique_flight_num = len(set(clean_data_dict['idf'])) - ccount
        ccount = len(set(clean_data_dict['idf']))
        print(f"{cdate} has {unique_flight_num} flights between this od.")
    print(f'**** Total {ccount} flights found during this time period. ****')
    # sort the data according to idf and time
    df = pd.DataFrame.from_dict(clean_data_dict)
    df = df.sort_values(by=['idf', 'time'])    # ascending order for each flight

    # add a new column `point_order`
    order_list = []
    idf_set = set()
    cur_num = start_label
    row_idx_list, df_value = df.index, df.values

    for cidx, _ in enumerate(row_idx_list):
        if df_value[cidx][0] not in idf_set:
            cur_num = start_label
            idf_set.add(df_value[cidx][0])
        order_list.append(cur_num)
        cur_num += 1

    order_ser = pd.Series(order_list)
    order_ser.index = df.index
    df['point_order'] = order_ser

    ## append geo-location of origin/destination to the specific data
    if airpInfor_dir:
        with open(airpInfor_dir, 'r') as fd:
            airpInfor = json.load(fd)
        od_geo_location_dict = {item: [] for item in cleaned_data_columns}
        od_geo_location_dict['point_order'] = []

        unique_flight = set(df['idf'].tolist())
        od_prog = re.compile(r'\((\w{3})\-(\w{3})')
        fid_prog = re.compile(r'(.*)_\(')
        reg_prog = re.compile(r'\(\w{3}\-\w{3}_(.*)\)')

        for cflight in unique_flight:
            od_reg = od_prog.search(cflight)
            cur_ori, cur_des = od_reg.group(1), od_reg.group(2)

            fid_reg = fid_prog.search(cflight)
            cur_fid = fid_reg.group(1)

            reg_reg = reg_prog.search(cflight)
            cur_reg = reg_reg.group(1)
            ## append origin aiport informaiton
            od_geo_location_dict['idf'].append(cflight)
            od_geo_location_dict['latitude'].append(airpInfor[cur_ori]['latitude'])
            od_geo_location_dict['longitude'].append(airpInfor[cur_ori]['longitude'])
            od_geo_location_dict['flightID'].append(cur_fid)
            od_geo_location_dict['origin'].append(cur_ori)
            od_geo_location_dict['destination'].append(cur_des)
            od_geo_location_dict['time'].append(0)
            od_geo_location_dict['point_order'].append(start_label-1)
            ## trajectory data
            cf_df = df[df['idf'] == cflight].values.tolist()
            for cidx in range(len(cf_df)):
                for inneridx, ccol in enumerate(df.columns):
                    od_geo_location_dict[ccol].append(cf_df[cidx][inneridx])
            ## append destination aiport informaiton
            coord_num = len(cf_df)
            od_geo_location_dict['idf'].append(cflight)
            od_geo_location_dict['latitude'].append(airpInfor[cur_des]['latitude'])
            od_geo_location_dict['longitude'].append(airpInfor[cur_des]['longitude'])
            od_geo_location_dict['flightID'].append(cur_fid)
            od_geo_location_dict['origin'].append(cur_ori)
            od_geo_location_dict['destination'].append(cur_des)
            od_geo_location_dict['time'].append(0)
            od_geo_location_dict['point_order'].append(start_label+coord_num)
        
        df = pd.DataFrame.from_dict(od_geo_location_dict)

    df.to_csv(os.path.join(store_dir, store_name), index=False)
    done_time = time.time()
    print('Total time consumed: {}'.format(done_time - pre_time))
    return df