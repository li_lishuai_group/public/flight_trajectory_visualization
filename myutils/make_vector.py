import numpy as np
import pandas as pd


def encode(resampled_traj):
    """ convert `resampled_traj` into a vector for clustering
    """
    idf_list = list(set(resampled_traj['idf'].tolist()))
    # find number of points per flight
    point_order_list = resampled_traj['point_order'].tolist()
    cN = np.max(point_order_list) - np.min(point_order_list) + 1
    # construct vector X
    X = np.zeros((len(idf_list), cN*2))
    for idx, cidf in enumerate(idf_list):
        cidf_data = resampled_traj[resampled_traj['idf'] == cidf]
        X[idx, ::2] = cidf_data['latitude'].values
        X[idx, 1::2] = cidf_data['longitude'].values
    
    return X, idf_list



def decode_centroid(X):
    """ convert vector `X` into a list of clustered routes
    """
    data_columns = ['idf', 'latitude', 'longitude', 'point_order']
    cN = int(X.shape[1] / 2)
    clustered_route_list = []
    centroid_num = 1

    for row_idx in range(len(X)):
        cur_centroid = np.zeros((cN, 2))
        cur_centroid[:, 0] = X[row_idx, ::2]
        cur_centroid[:, 1] = X[row_idx, 1::2]

        idf_array = np.array([f'centroid_{centroid_num}']*cN, dtype=object).reshape(-1, 1)
        point_order_array = np.arange(cN).reshape(-1, 1) + 1

        cur_clustered_centroid = np.concatenate((idf_array, cur_centroid, point_order_array), axis=1)
        clustered_route_list.append(pd.DataFrame(data=cur_clustered_centroid, columns=data_columns))
        centroid_num += 1
    
    return pd.concat(clustered_route_list)

def assign_clusters(X, clustering_labels, idf_list):
    """ assign cluster number to each flight trajectory
        X:                      vector of flight trajectory
        clustering_labels:      labels clusterd
        idf_list:               identification list
    """
    data_columns = ['idf', 'latitude', 'longitude', 'point_order']
    cN = int(X.shape[1] / 2)
    centroid_set = set(clustering_labels)
    clustered_route_dict = {str(cnum):[] for cnum in centroid_set}


    for row_idx, cur_idf, clabel in zip(range(len(X)), idf_list, clustering_labels):
        cur_traj = np.zeros((cN, 2))
        cur_traj[:, 0] = X[row_idx, ::2]
        cur_traj[:, 1] = X[row_idx, 1::2]

        idf_array = np.array([cur_idf]*cN, dtype=object).reshape(-1, 1)
        point_order_array = np.arange(cN).reshape(-1, 1) + 1

        cur_traj_full = np.concatenate((idf_array, cur_traj, point_order_array), axis=1)
        clustered_route_dict[str(clabel)].append(pd.DataFrame(data=cur_traj_full, columns=data_columns))
    
    return clustered_route_dict


