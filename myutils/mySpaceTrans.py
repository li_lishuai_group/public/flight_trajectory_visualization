import numpy as np
import pandas as pd
import math

""" do space transformation
"""

class geoSpace:
    """ store some initial data
    """
    def __init__(self, oriCoord=(np.zeros(3)), desCoord=np.zeros(3), trajArr=None, N=0):
        
        # raw data
        self.oriCoord = oriCoord
        self.desCoord = desCoord
        # add ``oriCoord``` and ``desCoord`` to trajArr
        self.trajArr = trajArr  
        # translated
        self.transCoord = None
        self.transTrajArr = None
        # rotated
        self.theta = None
        self.rotCoord2D = None
        self.rotTrajArr2D = None
        # scaled
        self.N = N
        self.scaleFa = None
        self.scaCoord2D = None
        self.scaTrajArr2D = None
    
    @staticmethod
    def coordinateDistance2D(coord1, coord2):
        """ calculate the distance of two coordinates. (do not consider ``altitude``)

            input:  coord1: (latitude, longitude)
                    coord2: (latitude, longitude)
            return: distance (unit: km)
        
        reference: 
        http://www.movable-type.co.uk/scripts/latlong.html
        https://stackoverflow.com/questions/19412462/getting-distance-between-two-points-based-on-latitude-longitude

        comment: maybe I should use ``geopy`` instead.
        """

        R = 6371.0      # radius of earth
        lat1, lon1 = map(math.radians, coord1)
        lat2, lon2 = map(math.radians, coord2)
        dlon = lon2 - lon1
        dlat = lat2 - lat1
        a = math.sin(dlat / 2) ** 2 + math.cos(lat1) * math.cos(lat2) * math.sin(dlon / 2) ** 2
        c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))
        return R * c

    @staticmethod
    def calShericalEarthProjection(coord1: tuple, coord2: tuple, R=6371.009) -> float:
        """ calcualte the geographical distance using ``Spherical earth projection to a plane``.
            see ``https://en.wikipedia.org/wiki/Geographical_distance``.

            coord1, coord2:  radian unit
            return:          kilometer
            Note: this is just an approximation method, but very quick. 
        """
        diffLat = coord1[0] - coord2[0]
        diffLon = coord1[1] - coord2[1]
        meanLat = (coord1[0] + coord2[0]) / 2
        return R * np.sqrt(diffLat**2 + (np.cos(meanLat) * diffLon)**2)

    @staticmethod
    def project2line2D(coord, twoPointsLine):
        """ project a point ``coord`` into a line ``twoPointLine``

            input: ``coord``: (lat, lon)
                   ``twoPointLine``: ((lat, lon), (lat, lon))
            
            return: ``projCoord``: projected point
                    ``proportion``: used for third axe (altitude)
        """
        baseLine = twoPointsLine[1] - twoPointsLine[0]
        transCoord = coord - twoPointsLine[0]
        # ``projCoord`` is the proportion of ``baseLine``
        # ``transCoord.dot(baseLine) / baseLine.dot(baseLine)`` is the proportion value..
        proportion = transCoord.dot(baseLine) / baseLine.dot(baseLine)
        projCoord = proportion * baseLine
        projCoord += twoPointsLine[0]
        return projCoord, proportion
    
    @staticmethod
    def computeIntersectionPoint():
        pass

    def translateSpace(self):
        """ translate the space (subtract the ``oriCoord``)

            **Note:** airpCoord is 2D
                      trackArr is 3D
        """
        self.transCoord = self.desCoord - self.oriCoord
        self.transTrajArr = self.trajArr - self.oriCoord
    
    def rotate2DSpace(self):
        """ rotate the translated space (multiply by a matrix)

            **Note:** just **2D rotation**. only the first 2 axes are considered.
        """
        # anti-clockwise is positive
        self.theta = math.atan2(self.transCoord[1], self.transCoord[0])
        # rote the space clockwise
        rotationMatrix = np.array([[math.cos(self.theta), math.sin(self.theta)], 
                                    [-math.sin(self.theta), math.cos(self.theta)]])
        self.rotCoord2D = np.matmul(rotationMatrix, self.transCoord[:2])
        self.rotTrajArr2D = np.matmul(self.transTrajArr[:, :2], rotationMatrix.T)

    def scaleSpace(self, distPerDot=10):
        """ scale the rotated space (just X-axes)

            1. first, caluclate the distance between ``origin`` and ``destination``, to determine ``N`` (40-50km a point).
            2. scale ``self.rotCoord`` to be ``N``, and multiply this ``factor`` to all the data in ``self.rotTrajArr``.
        """
        # distCoord = geoSpace.coordinateDistance2D(self.oriCoord, self.desCoord)
        # self.N = int(distCoord // distPerDot)
        # make sure self.N is not zero!!  (There is not validation check here!)
        self.scaleFa = np.array([self.N / self.rotCoord2D[0], 1])     # only ``0`` axe is considered
        self.scaCoord2D = self.rotCoord2D * self.scaleFa
        self.scaTrajArr2D = self.rotTrajArr2D *self.scaleFa

    def resampling(self):
        """ re-sampling (this function is not well structed..)
        """

        def findClosestInd(rs, rawDots):
            """ find the index of closest points within ``rawDots`` to ``rs`` 

                **Note:** rawDots is one dimentional data..
            """
            rawDotsWithInd = np.hstack(((rawDots-rs).reshape((-1, 1)), np.arange(rawDots.shape[0]).reshape((-1, 1))))
            nInd, nVal, pInd, pVal = -1, -np.Inf, -1, np.Inf

            exactlyZero = list(filter(lambda x: np.abs(x[0])<1e-6, rawDotsWithInd))
            if exactlyZero:
                ind = int(exactlyZero[0][1])
                return ind, ind
            
            lessThanZero = list(filter(lambda x: x[0]<0.0, rawDotsWithInd))
            greaterThanZero = list(filter(lambda x: x[0]>0.0, rawDotsWithInd))

            if lessThanZero and greaterThanZero:
                nInd = sorted(lessThanZero, key=lambda x: x[0])[-1][1]
                pInd = sorted(greaterThanZero, key=lambda x: x[0])[0][1]
            elif lessThanZero:
                nInd = sorted(lessThanZero, key=lambda x: x[0])[-1][1]
                pInd = nInd
            elif greaterThanZero:
                pInd = sorted(greaterThanZero, key=lambda x: x[0])[0][1]
                nInd = pInd
            else:   # both lessThanZero and greaterThanzero are empty.. this cannot happen..
                raise Exception("find closest index, impossible things happend!")
            
            return int(nInd), int(pInd)


        self.translateSpace()
        self.rotate2DSpace()
        self.scaleSpace()
        # get the number of resampling data..., just look at the x-axis
        resampled = np.arange(self.N+1)     # [0~N]
        rawDots = self.scaTrajArr2D[:, 0]
        # for each resmapled point, find a pair of index (index_of_lower, index_of_upper)
        pairIndex = []
        for rs in resampled:
            nInd, pInd = findClosestInd(rs, rawDots)
            pairIndex.append((nInd, pInd))      

        
        resampledTrack = []
        # trajArrWithOD = np.vstack(())
        for idx, (nInd, pInd) in zip(resampled, pairIndex):
            if nInd == pInd:
                resampledTrack.append(self.trajArr[nInd])
            else:
                scaPoint1, scaPoint2 = rawDots[nInd], rawDots[pInd]
                # calculate the proportion... [1~N-1]
                proportion = (idx - scaPoint1) / (scaPoint2 - scaPoint1)
                intersection = proportion * (self.trajArr[pInd] - self.trajArr[nInd]) + self.trajArr[nInd]
                resampledTrack.append(intersection)
        # do not add ``oriCoord, desCoord`` to the resampled result!!!!
        return np.array(resampledTrack)

    

if __name__ == '__main__':
    myGeo = geoSpace(oriCoord=np.array([1,3, 0]), desCoord=np.array([5, 6, 0]), trajArr=np.array([[1.1, 3.1, 0], [1.4, 3.3, 1],
        [3.5, 4.6, 2], [4.5, 5.6, 3], [4.8, 5.9, 4]]), N=5)