import numpy as np
import pandas as pd
import re, json, time, os
from myutils.mySpaceTrans import geoSpace

def compute_record_num(clean_data):
    """ compute the average point number of flights between this od
        clean_data:     cleand data generated from previous step
    """
    od_aggre = clean_data[['origin', 'destination']].values.tolist()
    od_list = [tuple(sorted([cur_od[0], cur_od[1]])) for cur_od in od_aggre]
    od_list = list(set(od_list))
    length_dict = {'-'.join(cur_od): [] for cur_od in od_list}

    flight_idf_set = set(clean_data['idf'].values.tolist())
    od_prog = re.compile(r'\((\w{3})\-(\w{3})')
    for cidf in flight_idf_set:
        cur_len = clean_data[clean_data['idf'] == cidf].shape[0]
        od_res = od_prog.search(cidf)
        cur_od = '-'.join(sorted([od_res.group(1), od_res.group(2)]))
        length_dict[cur_od].append(cur_len)
    
    # take average
    for key in length_dict:
        length_dict[key] = int(np.mean(length_dict[key]).item())
    
    return length_dict


def perform_resampling(clean_data, length_dict, airpInfor, store_dir='clustering/resampled'):
    """ perform resampling 
        clean_data:     cleand data generated from previous step
        length_dict:    contains the average length of each OD pair
        airpInfor:      contains airport geography information
    """
    pre_time = time.time()
    for cod, cN in length_dict.items():
        cod_list = cod.split('-')
        resampled_traj_list = []
        # find all the records of this od pair
        cur_flight_data = clean_data[(clean_data['origin'].isin(cod_list)) & 
                                    (clean_data['destination'].isin(cod_list))]
        # find a flight trajectory, and resample it
        flight_idf = set(cur_flight_data['idf'].values.tolist())
        for cfidf in flight_idf:
            one_flight_traj = cur_flight_data[cur_flight_data['idf'] == cfidf]
            # get the geography location of origin/destination airport
            d_ori, d_des = one_flight_traj['origin'].iloc[0], one_flight_traj['destination'].iloc[0]
            oriCoord = np.array([airpInfor[d_ori]['latitude'], airpInfor[d_ori]['longitude'], 0])
            desCoord = np.array([airpInfor[d_des]['latitude'], airpInfor[d_des]['longitude'], 0])
            trajArr = np.zeros((one_flight_traj.shape[0], 3))
            trajArr[:, :-1] = one_flight_traj[['latitude', 'longitude']].values
            # perform resampling
            cur_geo = geoSpace(oriCoord=oriCoord, desCoord=desCoord, trajArr=trajArr, N=cN)
            resmpled_traj = cur_geo.resampling()
            # different direction, then reverse it
            f_direc = '-'.join([d_ori, d_des])
            if f_direc != cod:
                resmpled_traj = resmpled_traj[::-1, :]
            # add `point order` and `flight identification` columns
            point_order_list = np.arange(len(resmpled_traj)).reshape(-1, 1) + 1
            fidf = np.array([cfidf]*len(resmpled_traj), dtype=object).reshape(-1, 1)
            resampled_traj_final = np.concatenate((fidf, resmpled_traj[:,:-1], point_order_list), axis=1)
            resampled_traj_list.append(resampled_traj_final)
        # store the results..
        resampled_traj_all = np.concatenate(resampled_traj_list, axis=0)
        cod_df = pd.DataFrame(data=resampled_traj_all, columns=['idf', 'latitude', 'longitude', 'point_order'])
        if not os.path.exists(store_dir):
            os.makedirs(store_dir)
        cod_df.to_csv(os.path.join(store_dir, f'{cod}.csv'), index=False)

    cur_time = time.time()
    print(f'Total time consumed: {cur_time - pre_time}')  