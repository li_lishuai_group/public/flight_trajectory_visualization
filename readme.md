# Flight Trajectory Visulization / Clustering / Regression

This project contains three parts:

1. flight trajectory visualization
2. flight trajectory clustering
3. estimate fuel consumption

## 1. flight trajectory visualization
Visualize flight trajectories using folium. It contains three steps:

1. Collect ADS-B data in json format,
2. Data pre-processing
    - convert json -> csv format
    - sample the data
3. visualization generation

## 2. flight trajectory clustering
Clustering using K-Means and DBSCAN. It contains three steps:

1. resampling the trajectory first
2. K-Means clustering
3. DBSCAN clustering

## 3. estimate fuel consumption
estimate fuel consumption using least-square regression.

## How to run

- dataset: ADS-B data of China's 56 busiest airports on Nov 1-3, 2016.
- Check the corresponding jupyter notebook for more details.

## Developer

Lin Yu (yulin8-c@my.cityu.edu.hk, City University of Hong Kong)

Lishuai Li (lishuai.li@cityu.edu.hk)
